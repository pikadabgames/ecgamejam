﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Player
{
	Left = 0,
	Right = 1,
}

public enum GameState
{
	Countdown = 0,
	GameActive = 1,
	GameOver = 2,
}
public class GameController : MonoBehaviour {
	public float gameTime;
	public float warningTimer = 10;
	public float initialCountdownTimer = 3;
	private float currentCountdownTimer;
	private int currentCountdownSize;
	public float initialShiftTimer = 0;
	public float shiftTimerVariance = 0;
	public float shiftTimerChange = 0;
	private float currentShiftTimer = 0;
	private bool shiftActive;

	private float gameOverTimer = 10;

	private int[] points;
	private List<KeyCode>[] keys = new List<KeyCode>[2];

	private GameState gameState;

	private AudioSource musicBox;

	// Use this for initialization
	void Start () {
		points = new int[2] {0, 0};
		keys[(int)Player.Left] = KeyPicker.Refresh(Player.Left);
		keys[(int)Player.Right] = KeyPicker.Refresh(Player.Right);

		currentShiftTimer = ResetShiftTimer();
		shiftActive = false;

		ChangeState(GameState.Countdown);

		musicBox = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		switch(gameState)
		{
			// if we're in the Countdown state
			case GameState.Countdown:
				currentCountdownTimer -= Time.deltaTime;
				UIController.UpdateCountdown(currentCountdownTimer, currentCountdownTimer/initialCountdownTimer);

				if(currentCountdownTimer <= 0)
					ChangeState(GameState.GameActive);
				break;
			// if we're in the Active state
			case GameState.GameActive:
				gameTime -= Time.deltaTime;
				currentShiftTimer -= Time.deltaTime;
				UIController.UpdateTimer((int)(gameTime));

				ValidateKeys(Player.Left);
				ValidateKeys(Player.Right);

				if(!shiftActive && currentShiftTimer <= 0)
				{
					StartShift();
				}
				else if(shiftActive)
				{
					if(Input.GetKeyDown(KeyCode.LeftShift))
					{
						points[(int)Player.Left]++;
						EndShift();
					}
					if(Input.GetKeyDown(KeyCode.RightShift))
					{
						points[(int)Player.Right]++;
						EndShift();
					}
				}
				if(gameTime <= 0)
					ChangeState(GameState.GameOver);
				break;
			// if we're in the GameOver state
			case GameState.GameOver:
				gameOverTimer -= Time.deltaTime;
				if(gameOverTimer <= 0)
				{
					GetComponent<SceneController>().ToMainMenu();
				}
				break;
		}
	}

	void ValidateKeys(Player player)
	{
		int playerIndex = (int)player;
		List<bool> keysDown = new List<bool>();

		bool allDown = true;
		bool isDown = true;
		for(int i = 0; i < keys[playerIndex].Count; i++)
		{
			isDown = Input.GetKey(keys[playerIndex][i]);
			allDown = allDown && isDown;
			keysDown.Add(isDown);
		}

		UIController.UpdateTextColor(player, keysDown);

		if(allDown)
		{
			points[playerIndex]++;
			UIController.UpdateScore(player, points[playerIndex]);
			UIController.Pet(player);

			keys[playerIndex] = KeyPicker.Refresh(player);
		}
	}

	void StartShift()
	{
		UIController.ShowShift(true);
		shiftActive = true;
		currentShiftTimer = ResetShiftTimer() + shiftTimerChange;
		musicBox.Play();
	}

	void EndShift()
	{
		UIController.ShowShift(false);
		shiftActive = false;
		UIController.Dab();
	}

	float ResetShiftTimer()
	{
		return Random.Range(initialShiftTimer - shiftTimerVariance, initialShiftTimer + shiftTimerVariance);
	}

	void ChangeState(GameState newState)
	{
		switch(newState)
		{
			case GameState.Countdown:
				currentCountdownTimer = initialCountdownTimer;
				UIController.SetCountdownActive(true);
				UIController.SetGameUIActive(false);
				break;
			case GameState.GameActive:
				UIController.SetCountdownActive(false);
				UIController.SetGameUIActive(true);
				break;
			case GameState.GameOver:
				Player winner = points[(int)Player.Left] > points[(int)Player.Right] ? Player.Left : Player.Right;
				UIController.GameOver(winner);
				break;
		}

		gameState = newState;
	}
}
