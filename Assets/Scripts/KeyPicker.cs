﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct KeyList
{
	public List<KeyCode> keys;
}
public class KeyPicker : MonoBehaviour {

	public static KeyPicker instance;

	// the number of keys used
	public int numKeys = 2;

	// the buckets the keys are placed in.  Currently, we are going to try not to use keys from the same bucket
	public List<KeyList> keyBuckets;

	// the key codes the player needs to type
	private List<KeyCode>[] targetCodes = new List<KeyCode>[2];

	// the key codes that are currently being used
	private Dictionary<KeyCode, bool> usedCodes = new Dictionary<KeyCode, bool>();

	void Start()
	{
		instance = this;

		targetCodes[(int)Player.Left] = new List<KeyCode>();
		targetCodes[(int)Player.Right] = new List<KeyCode>();
	}

	// refreshes the keys that a player needs to press and returns a list with the necessary keys
	public static List<KeyCode> Refresh(Player player)
	{
		int playerIndex = (int)player;
		List<KeyCode> keys = instance.targetCodes[playerIndex];
		// clears out the old keys
		while(keys.Count > 0)
		{
			instance.usedCodes[keys[0]] = false;
			keys.Remove(keys[0]);
		}

		// making sure we use keys from different buckets
		int numBuckets = instance.keyBuckets.Count;
		bool[] bucketsUsed = new bool[numBuckets];
		int keysUsed = 0;
		while(keysUsed < instance.numKeys)
		{
			int currentBucket = Random.Range(0, numBuckets - 1);

			if(instance.numKeys > numBuckets && (keysUsed % numBuckets) == 0)
			{
				for(int i = 0; i < instance.numKeys; i++)
				{
					bucketsUsed[i] = false;
				}
			}

			if(bucketsUsed[currentBucket] == false)
			{
				bucketsUsed[currentBucket] = true;
				int keyIndex = Random.Range(0, instance.keyBuckets[currentBucket].keys.Count - 1);
				
				KeyCode currentKey = instance.keyBuckets[currentBucket].keys[keyIndex];
				if(!instance.usedCodes.ContainsKey(currentKey) || instance.usedCodes[currentKey] == false)
				{
					keys.Add(currentKey);
					
					if(instance.usedCodes.ContainsKey(currentKey))
						instance.usedCodes[currentKey] = true;
					else
						instance.usedCodes.Add(currentKey, true);

					keysUsed++;
				}
			}
		}
		UIController.UpdateKeys(player, keys);
		return keys;
	}

	public static List<KeyCode> GetKeysForPlayer(Player player)
	{
		return instance.targetCodes[(int)player];
	}
}
