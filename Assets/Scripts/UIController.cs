﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	//Making UIController a signleton
	private static UIController instance;

	//Creating a map that hardcodes the strings for specific keys
	private Dictionary<KeyCode, string> stringMap = new Dictionary<KeyCode, string>();

	//The windows that currently hold the score
	//public Text[] scoreWindows = new Text[2];

	//The windows that currently show the keys
	public Text[] keyWindows = new Text[2];

	public Text shiftWindow;

	//The window that shows the timer
	public Text timer;

	public Color[] textColors = new Color[2];

	public Color pressedColor;

	public GameObject activeUI;
	public GameObject countdownUI;
	public Text countdownText;
	public int countdownStartSize = 300;
	public int countdownFinalSize = 100;

	public GameObject gameOverUI;

	public GameObject leftHand;
	public GameObject rightHand;
	private Animator[] handAnimators = new Animator[2];

	public GameObject capsActiveGame;
	private Animator activeAnimator;
	public GameObject capsDecide;
	private Animator gameOverAnimator;

	private string[] textHexes  = new string[2];
	private string pressedHex;
	private AudioSource musicBox;

	void Start()
	{
		instance = this;

		// initializing strings for key codes.  This may be moved to Unity
		stringMap.Add(KeyCode.Alpha0, "0");
		stringMap.Add(KeyCode.Alpha1, "1");
		stringMap.Add(KeyCode.Alpha2, "2");
		stringMap.Add(KeyCode.Alpha3, "3");
		stringMap.Add(KeyCode.Alpha4, "4");
		stringMap.Add(KeyCode.Alpha5, "5");
		stringMap.Add(KeyCode.Alpha6, "6");
		stringMap.Add(KeyCode.Alpha7, "7");
		stringMap.Add(KeyCode.Alpha8, "8");
		stringMap.Add(KeyCode.Alpha9, "9");
		stringMap.Add(KeyCode.Comma, ",");
		stringMap.Add(KeyCode.Period, ".");
		stringMap.Add(KeyCode.Semicolon, ";");
		stringMap.Add(KeyCode.Slash, "/");

		textHexes[(int)Player.Left] = ColorUtility.ToHtmlStringRGBA(textColors[(int)Player.Left]);
		textHexes[(int)Player.Right] = ColorUtility.ToHtmlStringRGBA(textColors[(int)Player.Right]);

		pressedHex = ColorUtility.ToHtmlStringRGB(pressedColor);
		shiftWindow.enabled = false;

		activeUI.SetActive(false);
		countdownUI.SetActive(false);

		gameOverUI.SetActive(false);

		handAnimators[(int)Player.Left] = leftHand.GetComponent<Animator>();
		handAnimators[(int)Player.Right] = rightHand.GetComponent<Animator>();

		activeAnimator = capsActiveGame.GetComponent<Animator>();
		gameOverAnimator = capsDecide.GetComponent<Animator>();

		musicBox = GetComponent<AudioSource>();
		musicBox.Play();
	}


	public static void UpdateScore(Player player, int score)
	{
		//instance.scoreWindows[(int)player].text = score.ToString();
	}

	public static void UpdateKeys(Player player, List<KeyCode> keys)
	{

		string output = "";
		int playerKey = (int)player;

		foreach(KeyCode key in keys)
		{
			// checking if the key needs a custom string or not
			if(instance.stringMap.ContainsKey(key))
				output += instance.stringMap[key];
			else
				output += key.ToString();
		}

		// concatinating the strings
		instance.keyWindows[playerKey].text = output;

	}

	public static void UpdateTimer(int seconds)
	{
		int minutes = seconds/60;
		int trueSeconds = seconds%60;
		string printSeconds = trueSeconds.ToString();
		if(trueSeconds < 10)
		{
			printSeconds = "0" + printSeconds;
		}

		instance.timer.text = minutes + ":" + printSeconds;
	}

	public static void UpdateTextColor(Player player, List<bool> pressedStates)
	{
		int playerIndex = (int)player;
		string colorHex;
		List<KeyCode> keys = KeyPicker.GetKeysForPlayer(player);

		string newText = "";
		for(int i = 0; i < keys.Count; i++)
		{
			string keyString;
			if(instance.stringMap.ContainsKey(keys[i]))
				keyString = instance.stringMap[keys[i]];
			else
				keyString = keys[i].ToString();
			
			if(pressedStates[i])
				colorHex = instance.pressedHex;
			else
				colorHex = instance.textHexes[playerIndex];
				
			newText += "<color=#" + colorHex + ">" + keyString + "</color>";
		}

		instance.keyWindows[playerIndex].text = newText;
	}

	public static void ShowShift(bool show)
	{
		instance.shiftWindow.enabled = show;
	}

	public static void UpdateCountdown(float time, float percentCountdownComplete)
	{
		instance.countdownText.text = ((int)time).ToString();
		instance.countdownText.fontSize = (int)(instance.countdownStartSize - ((instance.countdownStartSize - instance.countdownFinalSize) * (1 - percentCountdownComplete)));
	}

	public static void SetCountdownActive(bool isActive)
	{
		instance.countdownUI.SetActive(isActive);
	}

	public static void SetGameUIActive(bool isActive)
	{
		instance.activeUI.SetActive(isActive);
	}

	public static void GameOver(Player player)
	{
		instance.gameOverUI.SetActive(true);
		if(player == Player.Left)
		{
			instance.gameOverAnimator.SetTrigger("LeftWins");
		}
		else
		{
			instance.gameOverAnimator.SetTrigger("RightWins");
		}
		instance.musicBox.Stop();
	}

	public static void Pet(Player player)
	{
		if(player == Player.Left)
			instance.handAnimators[(int)player].SetTrigger("LeftScored");
		else
			instance.handAnimators[(int)player].SetTrigger("RightScored");
	}

	public static void Dab()
	{
		instance.activeAnimator.SetTrigger("Dab");
	}
}
