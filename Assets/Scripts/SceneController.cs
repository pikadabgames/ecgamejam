﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public enum GameScene 
{
	MainMenu = 0,
	Game = 1,
	EndGame = 2,
}

public class SceneController : MonoBehaviour {
	private Dictionary<GameScene, string> sceneMap = new Dictionary<GameScene, string>();

	void Start()
	{
		sceneMap.Add(GameScene.MainMenu, "MainMenu");
		sceneMap.Add(GameScene.Game, "MainGame");
	}

	public void ToMainMenu()
	{
		SceneManager.LoadScene(sceneMap[GameScene.MainMenu]);
	}

	public void ToMainGame()
	{
		SceneManager.LoadScene(sceneMap[GameScene.Game]);
	}	
}
