﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUI : MonoBehaviour {

	private AudioSource soundSource;
	public GameObject creditsWindow;
	public GameObject instructionsWindow;
	// Use this for initialization
	void Start () {
		creditsWindow.SetActive(false);
		instructionsWindow.SetActive(false);
	}

	public void ShowCredits()
	{
		creditsWindow.SetActive(true);
	}

	public void HideCredits()
	{
		creditsWindow.SetActive(false);
	}

	public void ShowTut()
	{
		instructionsWindow.SetActive(true);
	}

	public void HideTut()
	{
		instructionsWindow.SetActive(false);
	}
}
